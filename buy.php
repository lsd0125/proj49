<?php
require __DIR__ . '/__connect_db.php';
$pname = 'buy';

if(! isset($_SESSION['user']) or empty($_SESSION['cart'])) {
    header('Location: product_list.php');
    exit;
}

$amount = 0;


$keys = array_keys($_SESSION['cart']);

$sql = sprintf("SELECT * FROM `products` WHERE `sid` IN (%s)", implode(',', $keys));

$rs = $mysqli->query($sql);

while($row = $rs->fetch_assoc()){
    $row['qty'] = $_SESSION['cart'][$row['sid']]; // 取得某項商品的數量
    $c_prod[ $row['sid'] ] = $row;

    $amount += $row['qty']*$row['price'];
}


$sql = sprintf("INSERT INTO `orders`(`member_sid`, `amount`, `order_date`) VALUES (%s, %s, NOW())",
        $_SESSION['user']['id'],
        $amount
    );

$mysqli->query($sql);

$insert_id = $mysqli->insert_id; //拿到最新一筆資料的 主鍵


foreach($c_prod as $p){

    $sql = sprintf("INSERT INTO `order_details`(
        `order_sid`, `product_sid`, `price`, `quantity`
        ) VALUES (
          %s, %s, %s, %s
        )",
        $insert_id,
        $p['sid'],
        $p['price'],
        $p['qty']
        );

    $mysqli->query($sql);
}

unset($_SESSION['cart']);
?>

<?php include __DIR__. '/__page_head.php' ?>

<div class="container">
    <?php include __DIR__. '/__navbar.php' ?>


        <div class="col-md-12">
            <div class="alert alert-success" role="alert">感謝您的訂購, 訂單請查看歷史記錄</div>
        </div>



</div>
    <script>

    </script>
<?php include __DIR__. '/__page_foot.php' ?>