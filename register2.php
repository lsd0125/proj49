<?php
require __DIR__ . '/__connect_db.php';
$pname = 'register2';


?>
<?php include __DIR__ . '/__page_head.php' ?>
    <style>
        .red {
            color: red;
            display: none;
        }
    </style>
    <div class="container">
        <?php include __DIR__ . '/__navbar.php' ?>

        <div class="row">
            <div class="col-md-6">

                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">會員註冊</h3></div>
<!--                    <div>--><?php //echo hash('sha256', 'dfdfgdfgdd') ?><!--</div>-->
                    <div class="panel-body">

                        <form name="form1" method="post" onsubmit="return checkForm();">
                            <div class="form-group">
                                <label for="email">**電郵帳號</label> <span class="red">請填寫正確的 email</span>

                                <input type="text" class="form-control" id="email" name="email" placeholder="請輸入你的Email"
                                >
                            </div>
                            <div class="form-group">
                                <label for="password">**密碼</label> <span class="red">請填寫密碼</span>
                                <input type="password" class="form-control" id="password" name="password">

                            </div>
                            <div class="form-group">
                                <label for="nickname">**暱稱</label> <span class="red">請填寫暱稱</span>
                                <input type="text" class="form-control" id="nickname" name="nickname" placeholder="暱稱">

                            </div>
                            <div class="form-group">
                                <label for="mobile">手機</label>
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="">

                            </div>

                            <div class="form-group">
                                <label for="address">地址</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="">

                            </div>
                            <div class="form-group">
                                <label for="birthday">生日</label>
                                <input type="text" class="form-control" id="birthday" name="birthday" placeholder="">

                            </div>

                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </form>
                    </div>
                </div>


            </div>
        </div>

        <div>
            <p><a class='inline' href="#inline_content" style="display: none">Inline HTML</a></p>
        </div>

        <div style='display:none'>
            <div id='inline_content' style='padding:10px; background:#fff;'>
                <p>您好</p>
            </div>
        </div>
    </div>

    <script>
        var inline_content = $('#inline_content');

        $('#birthday').datepicker({
            dateFormat: "yy-mm-dd"
        });
        function checkForm(){

            var isPass = true;
            var email = $('#email');
            var password = $('#password');
            var nickname = $('#nickname');

            email.prev().hide();
            password.prev().hide();
            nickname.prev().hide();

            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            if(! pattern.test(email.val())){
                email.prev().show();
                isPass = false;
            }


            if(password.val().length < 3 ){
                password.prev().show();
                isPass = false;
            }

            if(nickname.val().length < 2 ){
                nickname.prev().show();
                isPass = false;
            }

            if(isPass){

                $.post('register2_back.php', $(document.form1).serialize(), function(data){
                    //alert(data.info);
                    var div = $('<div></div>');
                    div.html(data.info);
                    if(data.success){
                        div.css('color', 'green');
                    } else{
                        div.css('color', 'red');

                    }

                    inline_content.html(div);

                    $(".inline").click();
                }, 'json');



            }



            return false;
        }

        $(".inline").colorbox({inline:true, width:"50%"});


    </script>
<?php include __DIR__ . '/__page_foot.php' ?>