<?php
include __DIR__. '/__connect_db.php';

$m_id = isset($_GET['m_id']) ? intval($_GET['m_id']) : 0;

$sql = "SELECT * FROM `orders`
WHERE `member_sid`=$m_id
ORDER BY `order_date` DESC;";

$rs = $mysqli->query($sql);

$data = array();
$seq = array();

while($r1=$rs->fetch_assoc()){
    $seq[] = $r1['sid']; // push
    $order_sid = $r1['sid']; //訂單編號
    $sql2 = "SELECT
  d.product_sid,
  d.price,
  d.quantity,
  p.bookname
FROM  `order_details` d
  JOIN `products` p
    ON d.product_sid=p.sid
WHERE d.order_sid=$order_sid";

    $rs2 = $mysqli->query($sql2);

    $r1['data'] = $rs2->fetch_all(MYSQLI_ASSOC);
    $data[ $r1['sid'] ] = $r1;
}

echo json_encode(array(
    'success' => true,
    'data_seq' => $seq,
    'data' => $data,


), JSON_UNESCAPED_UNICODE);


