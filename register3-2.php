<?php
require __DIR__ . '/__connect_db.php';


if( isset($_POST['email']) ){

    $hash = sha1($_POST['email']. uniqid());
    $sql = "INSERT INTO `members`(
         `email`,
         `password`, 
         `mobile`, 
         `address`, 
         `birthday`, 
         `nickname`, 
         `hash`, 
         `created_at`
          ) VALUES ( ?, ?, ?, ?, ?, ?, '$hash', NOW() )";
    
    $stmt = $mysqli->prepare($sql);
    if($mysqli->error){
        echo $mysqli->error;
        exit;
    }
    $stmt->bind_param("ssssss",
        $_POST['email'] ,
        sha1($_POST['password']) ,
        $_POST['mobile'],

        $_POST['address'],
        $_POST['birthday'],
        $_POST['nickname']
    );

    $stmt->execute();

/*
    if($stmt->error){
        echo $stmt->error;
        exit;
    }
*/
    //echo $sql;
    $result = $stmt->affected_rows;

//    echo $result;
//    exit;
}


?>
<?php include __DIR__ . '/__page_head.php' ?>

    <div class="container">

        <?php if(isset($result)): ?>
            <?php if($result==1): ?>
                <div class="col-md-12" id="myinfo">
                    <div class="alert alert-success" role="alert">
                        資料新增完成
                    </div>
                </div>

            <?php elseif($result==-1): ?>
                <div class="col-md-12" id="myinfo">
                    <div class="alert alert-danger" role="alert">
                        email 已經被使用過了
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>


<?php include __DIR__ . '/__page_foot.php' ?>