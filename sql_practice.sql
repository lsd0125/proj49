SELECT * FROM `products` JOIN `categories`;


SELECT * FROM `products` JOIN categories ON `products`.`category_sid`=`categories`.`sid`;

SELECT * FROM `categories` JOIN `products` ON `products`.`category_sid`=`categories`.`sid`;

SELECT `products`.*, `categories`.`name` FROM `products`
  JOIN `categories`
  ON `products`.`category_sid`=`categories`.`sid`;


SELECT `products`.`bookname`, `categories`.`name` FROM `products`
  JOIN `categories`
  ON `products`.`category_sid`=`categories`.`sid`;


SELECT p.*, c.`name`
  FROM `products` AS p
  JOIN `categories` AS c
    ON p.`category_sid`=c.`sid`;

SELECT p.*, c.`name` `分類別稱`
FROM `products` p
  JOIN `categories` c
    ON p.`category_sid`=c.`sid`;

-- --------------------
SELECT p.sid, p.`bookname`, c.`name` `分類別稱`
FROM `products` p
  JOIN `categories` c
    ON p.`category_sid`=c.`sid`;


SELECT p.sid, p.`bookname`, c.`name` `分類別稱`
FROM `categories` c
  JOIN `products` p
    ON p.`category_sid`=c.`sid`;

-- -------------------- LEFT JOIN ----
SELECT p.sid, p.`bookname`, c.`name` `分類別稱`
FROM `products` p
  LEFT JOIN `categories` c
    ON p.`category_sid`=c.`sid`;


SELECT p.sid, p.`bookname`, c.`name` `分類別稱`
FROM `categories` c
  LEFT JOIN `products` p
    ON p.`category_sid`=c.`sid`;
-- --------------------
SELECT * FROM `products` WHERE `author` LIKE '田';

SELECT * FROM `products` WHERE `author` LIKE '%田%';

SELECT * FROM `products`
  WHERE `author` LIKE '%田%'
        OR `bookname` LIKE '%田%';

-- -------------------- 訂單相關 ---
SELECT * FROM `order_details` WHERE `order_sid`=11;

-- 某筆訂單的內容商品
SELECT d.*, p.bookname FROM `order_details` d
    JOIN `products` p
    ON d.product_sid = p.sid
    WHERE `order_sid`=11;


SELECT d.*, p.bookname, o.order_date
  FROM `order_details` d
  JOIN `products` p
    ON d.product_sid = p.sid
  JOIN `orders` o
    on d.order_sid=o.sid
  WHERE d.`order_sid`=11;

SELECT `category_sid`, COUNT(*) num
  FROM `products`
  GROUP BY `category_sid`;


SELECT p.`category_sid`, COUNT(*) num, c.name
  FROM `products` p
    JOIN `categories` c
    ON p.category_sid=c.sid
  GROUP BY p.`category_sid`;


SELECT * FROM `orders` WHERE `order_date` < '2016-06-01';

SELECT * FROM `orders`
  WHERE `order_date` >= '2016-06-01'
  AND `order_date` < '2016-06-02';


-- 某個會員的訂單歷史記錄
SELECT * FROM `orders`
  WHERE `member_sid`=1
  ORDER BY `order_date` DESC;


SELECT *
FROM `orders` o
  JOIN `order_details` d
    ON o.sid=d.order_sid
WHERE o.`member_sid`=1
ORDER BY o.`order_date` DESC;


SELECT
  o.amount,
  o.order_date,
  d.order_sid,
  d.product_sid,
  d.price,
  d.quantity
FROM `orders` o
  JOIN `order_details` d
    ON o.sid=d.order_sid
WHERE o.`member_sid`=1
ORDER BY o.`order_date` DESC;

SELECT
  o.amount,
  o.order_date,
  d.order_sid,
  d.product_sid,
  d.price,
  d.quantity,
  p.bookname
FROM `orders` o
  JOIN `order_details` d
    ON o.sid=d.order_sid
  JOIN `products` p
    ON d.product_sid=p.sid
WHERE o.`member_sid`=1
ORDER BY o.`order_date` DESC;


