<?php
require __DIR__ . '/__connect_db.php';
$pname = 'register';


if( isset($_POST['email']) ){

    $hash = sha1($_POST['email']. uniqid());
    $sql = "INSERT INTO `members`(
         `email`,
         `password`, 
         `mobile`, 
         `address`, 
         `birthday`, 
         `nickname`, 
         `hash`, 
         `created_at`
          ) VALUES ( ?, ?, ?, ?, ?, ?, '$hash', NOW() )";
    
    $stmt = $mysqli->prepare($sql);
    if($mysqli->error){
        echo $mysqli->error;
        exit;
    }
    $stmt->bind_param("ssssss",
        $_POST['email'] ,
        sha1($_POST['password']) ,
        $_POST['mobile'],

        $_POST['address'],
        $_POST['birthday'],
        $_POST['nickname']
    );

    $stmt->execute();

/*
    if($stmt->error){
        echo $stmt->error;
        exit;
    }
*/
    //echo $sql;
    $result = $stmt->affected_rows;

//    echo $result;
//    exit;
}


?>
<?php include __DIR__ . '/__page_head.php' ?>
    <style>
        .red {
            color: red;
            display: none;
        }
    </style>
    <div class="container">
        <?php include __DIR__ . '/__navbar.php' ?>

        <?php if(isset($result)): ?>
            <?php if($result==1): ?>
                <div class="col-md-12" id="myinfo">
                    <div class="alert alert-success" role="alert">
                        資料新增完成
                    </div>
                </div>

                <?php elseif($result==-1): ?>
            <div class="col-md-12" id="myinfo">
                <div class="alert alert-danger" role="alert">
                    email 已經被使用過了
                </div>
            </div>
                <?php endif; ?>
            <script>
                setTimeout(function(){
                    $('#myinfo').slideUp();
                }, 3000);
            </script>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-6">

                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">會員註冊</h3></div>
<!--                    <div>--><?php //echo hash('sha256', 'dfdfgdfgdd') ?><!--</div>-->
                    <div class="panel-body">

                        <form method="post" onsubmit="return checkForm();">
                            <div class="form-group">
                                <label for="email">**電郵帳號</label> <span class="red">請填寫正確的 email</span>

                                <input type="text" class="form-control" id="email" name="email" placeholder="請輸入你的Email"
                                value="<?= empty($_POST['email']) ? '' : htmlentities($_POST['email']) ?>">
                            </div>
                            <div class="form-group">
                                <label for="password">**密碼</label> <span class="red">請填寫密碼</span>
                                <input type="password" class="form-control" id="password" name="password"
                                       value="<?= empty($_POST['password']) ? '' : htmlentities($_POST['password']) ?>">

                            </div>
                            <div class="form-group">
                                <label for="nickname">**暱稱</label> <span class="red">請填寫暱稱</span>
                                <input type="text" class="form-control" id="nickname" name="nickname" placeholder="暱稱"
                                       value="<?= empty($_POST['nickname']) ? '' : htmlentities($_POST['nickname']) ?>">

                            </div>
                            <div class="form-group">
                                <label for="mobile">手機</label>
                                <input type="text" class="form-control" id="mobile" name="mobile" placeholder=""
                                       value="<?= empty($_POST['mobile']) ? '' : htmlentities($_POST['mobile']) ?>">

                            </div>

                            <div class="form-group">
                                <label for="address">地址</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder=""
                                       value="<?= empty($_POST['address']) ? '' : htmlentities($_POST['address']) ?>">

                            </div>
                            <div class="form-group">
                                <label for="birthday">生日</label>
                                <input type="text" class="form-control" id="birthday" name="birthday" placeholder=""
                                       value="<?= empty($_POST['birthday']) ? '' : htmlentities($_POST['birthday']) ?>">

                            </div>

                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <script>
        $('#birthday').datepicker({
            dateFormat: "yy-mm-dd"
        });
        function checkForm(){

            var isPass = true;
            var email = $('#email');
            var password = $('#password');
            var nickname = $('#nickname');

            email.prev().hide();
            password.prev().hide();
            nickname.prev().hide();

            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            if(! pattern.test(email.val())){
                email.prev().show();
                isPass = false;
            }


            if(password.val().length < 3 ){
                password.prev().show();
                isPass = false;
            }

            if(nickname.val().length < 2 ){
                nickname.prev().show();
                isPass = false;
            }

            return isPass;
        }




    </script>
<?php include __DIR__ . '/__page_foot.php' ?>