<?php
include __DIR__. '/__connect_db.php';

$m_id = isset($_GET['m_id']) ? intval($_GET['m_id']) : 0;

$sql = "SELECT
  o.amount,
  o.order_date,
  d.order_sid,
  d.product_sid,
  d.price,
  d.quantity,
  p.bookname
FROM `orders` o
  JOIN `order_details` d
    ON o.sid=d.order_sid
  JOIN `products` p
    ON d.product_sid=p.sid
WHERE o.`member_sid`=$m_id
ORDER BY o.`order_date` DESC;";

$rs = $mysqli->query($sql);

$data =  $rs->fetch_all(MYSQLI_ASSOC);

echo json_encode($data, JSON_UNESCAPED_UNICODE);


