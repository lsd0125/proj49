<?php
require __DIR__ . '/__connect_db.php';
$pname = 'product_list';

$c_rs = $mysqli->query("SELECT * FROM `categories` WHERE `parent_sid`=0 ");
while($r=$c_rs->fetch_assoc()){
    $cates[ $r['sid'] ] = $r;
}

$sid = isset($_GET['sid']) ? (int)$_GET['sid'] : 0;

$sql = "SELECT * FROM `products` WHERE `sid`=$sid";
$rs = $mysqli->query($sql);
$row = $rs->fetch_assoc();

?>
<?php include __DIR__. '/__page_head.php' ?>

<div class="container">
    <?php include __DIR__. '/__navbar.php' ?>


        <div class="col-md-12">
            <div class="col-md-3">
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="product_list.php?cate=0">所有商品</a>
                    </li>
                    <?php foreach($cates as $c): ?>
                    <li class="list-group-item">

                        <a href="product_list.php?cate=<?= $c['sid'] ?>">
                        <?= $c['name'] ?>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>


            </div>
            <div class="col-md-9">
                <div class="col-md-12">
                    <?php if(empty($row)): ?>
                        <div class="alert alert-danger" role="alert">沒有商品資料</div>
                    <?php else: ?>

                        <div class="thumbnail">
                            <img src="imgs/big/<?= $row['book_id'] ?>.png" style="width: 180px; height: 240px;">
                            <div class="caption">
                                <h5><?= $row['bookname'] ?></h5>
                                <h5><?= $row['author'] ?></h5>
                                <p>
                                    <span class="glyphicon glyphicon-search"></span>
                                    <span class="label label-info">$ <?= $row['price'] ?></span>
                                    <select name="qty" class="qty">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                    <button class="btn btn-warning btn-sm buy_btn" data-sid="1">買</button>
                                </p>
                                <p>
                                    <?= $row['introduction'] ?>

                                </p>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
</div>
    <script>


    </script>
<?php include __DIR__. '/__page_foot.php' ?>