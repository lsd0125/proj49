<?php
require __DIR__ . '/__connect_db.php';
$pname = 'product_list';

$c_rs = $mysqli->query("SELECT * FROM `categories` WHERE `parent_sid`=0 ");
while($r=$c_rs->fetch_assoc()){
    $cates[ $r['sid'] ] = $r;
}
/*
$c_rs->data_seek(0);

while($r=$c_rs->fetch_assoc()){
    $cates[] = $r;
}
*/
$cate = isset($_GET['cate']) ? (int)$_GET['cate'] : 0;
$has_cate = !empty( $cates[$cate] ); //有沒有這個分類

//echo $has_cate ? '有' : '無';

$p_where = ' WHERE 1 ';
if($has_cate) {
    $p_where .= " AND `category_sid`=$cate ";
}

//print_r($cates);
// 取得總筆
$t_rs = $mysqli->query("SELECT COUNT(1) FROM `products` $p_where");

$t_row = $t_rs->fetch_row();
$num_rows = $t_row[0];

// 取得該頁的資料
$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
$per_page = 4;
$num_pages = ceil($num_rows/$per_page);

$sql = sprintf("SELECT * FROM `products` $p_where LIMIT %s, %s", ($page-1)*$per_page, $per_page);

$rs = $mysqli->query($sql);

// -------

$c2_rs = $mysqli->query("SELECT * FROM `categories` ");
while($r2=$c2_rs->fetch_assoc()){
    $cates2[] = $r2;
}

$mmenu = array();
foreach($cates2 as $c) {
    if($c['parent_sid']==0){
        $mmenu[$c['sid']] = $c;
    }
}
foreach($cates2 as $c) {
    if(!empty( $mmenu[$c['parent_sid']] )){
        $mmenu[$c['parent_sid']]['sub'][] = $c;
    }
}

//print_r($mmenu);
//exit;



?>
<?php include __DIR__. '/__page_head.php' ?>

<div class="container">
    <?php include __DIR__. '/__navbar.php' ?>


        <div class="col-md-12">
            <div class="col-md-3">
                <ul class="list-group">
                    <li class="list-group-item <?= !$has_cate ? 'active' : '' ?>">
                        <a href="?cate=0">所有商品</a>
                    </li>
                    <?php foreach($cates as $c): ?>
                    <li class="list-group-item <?= $cate==$c['sid'] ? 'active' : '' ?>">

                        <a href="?cate=<?= $c['sid'] ?>">
                        <?= $c['name'] ?>
                        </a>
                    </li>
                    <?php endforeach; ?>
                </ul>

                <?php foreach($mmenu as $m): ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="true"><?= $m['name'] ?> <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <?php foreach($m['sub'] as $n): ?>
                        <li><a href="?cate=<?= $n['sid'] ?>"><?= $n['name'] ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div><br>
                <?php endforeach; ?>
            </div>
            <div class="col-md-9">
                <div class="col-md-12">
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <?php for($i=1; $i<=$num_pages; $i++):

                                $qr = array(
                                    'page' => $i,
                                );
                                if($has_cate) {
                                    $qr['cate'] = $cate;
                                }
                                ?>
                            <li class="<?= $page==$i ? 'active' : '' ?>">
                                <a href="?<?= http_build_query($qr) ?>"><?=$i?></a>
                            </li>
                            <?php endfor ?>

                        </ul>
                    </nav>
                </div>


                <?php while($row=$rs->fetch_assoc()): ?>

                    <div class="col-md-3">
                        <div class="thumbnail" style="height:280px; margin:10px 0;">
                            <a class="single_product" href="single-product.php?sid=<?= $row['sid'] ?>">
                                <img src="imgs/small/<?= $row['book_id'] ?>.jpg" style="width: 100px; height: 135px;">
                            </a>
                            <div class="caption">
                                <h5><?= $row['bookname'] ?></h5>
                                <h5><?= $row['author'] ?></h5>
                                <p>
                                    <span class="glyphicon glyphicon-search"></span>
                                    <span class="label label-info">$ <?= $row['price'] ?></span>
                                    <select name="qty" class="qty">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                    <button class="btn btn-warning btn-sm buy_btn" data-sid="<?= $row['sid'] ?>">買</button>
                                </p>
                            </div>
                        </div>
                    </div>

                <?php endwhile; ?>


            </div>



        </div>



</div>
    <script>
        $('.buy_btn').click(function(){
            var sid = $(this).attr('data-sid');
            var qty = $(this).closest('.thumbnail').find('.qty').val();
            var bookname = $(this).closest('.thumbnail').find('h5').eq(0).text();

            $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
                alert(bookname + ' 已加入購物車');
                calItems(data); // 計算並顯示總數量
            }, 'json');


        });

    </script>
<?php include __DIR__. '/__page_foot.php' ?>