<div class="row">
    <div class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="collapsed navbar-toggle" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                    </button>
                    <a href="./" class="navbar-brand">小新的店</a></div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="<?= $pname=='product_list' ? 'active' : '' ?>"><a href="product_list.php">商品列表</a></li>
                        <li class="<?= $pname=='cart_list' ? 'active' : '' ?>"><a href="cart_list.php">購物車<span class="badge t-badge"></span></a></li>

                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <?php if(isset($_SESSION['user'])): ?>
                            <li class=""><a><?= $_SESSION['user']['nickname'] ?></a></li>
                            <li class="<?= $pname=='history' ? 'active' : '' ?>"><a href="history.php">訂單記錄</a></li>

                            <li class=""><a href="logout.php">登出</a></li>
                        <?php else: ?>
                            <li class="<?= $pname=='login' ? 'active' : '' ?>"><a href="login.php">登入</a></li>
                            <li class="<?= $pname=='register' ? 'active' : '' ?>"><a href="register.php">註冊</a></li>
                            <li class="<?= $pname=='register2' ? 'active' : '' ?>"><a href="register2.php">註冊ajax</a></li>
                            <li class=""><a class="register3" href="register3.php">註冊iframe</a></li>
                            <script>
                                $('.register3').colorbox({iframe:true, innerWidth:600, innerHeight:600});
                            </script>
                        <?php endif; ?>


                    </ul>
                </div>
            </div>
        </nav>

    </div>
</div>
<script>
    $.get('add_to_cart.php', function(data){
        calItems(data);
    }, 'json');

    function calItems(obj){
        var total = 0;
        for(var s in obj){
            total += obj[s];
        }

        $('.t-badge').text( total);
    }

</script>