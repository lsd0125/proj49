<?php
require __DIR__ . '/__connect_db.php';
$pname = 'login';


if( isset($_POST['email']) ){
    $sql = sprintf("SELECT * FROM `members` WHERE `email`='%s' AND `password`='%s'",
        $mysqli->escape_string($_POST['email']),
        sha1($_POST['password'])
        );

    //echo $sql;

    $rs = $mysqli->query($sql);
    $row = $rs->fetch_assoc();
    if($row){
        $_SESSION['user'] = $row;
        //$msg = '登入成功';

//        header("Location: ./");

        if(isset($_SESSION['come_from'])){
            header("Location: ". $_SESSION['come_from']);
            unset($_SESSION['come_from']);
        } else {
            header("Location: ./");
        }
        exit;
    } else {
        $msg = '密碼錯誤';
    }
} else {
    $_SESSION['come_from'] = $_SERVER['HTTP_REFERER'];
}


?>
<?php include __DIR__ . '/__page_head.php' ?>
    <style>
        .red {
            color: red;
            display: none;
        }
    </style>
    <div class="container">
        <?php include __DIR__ . '/__navbar.php' ?>

        <?php if(isset($msg)): ?>
            <div class="col-md-12" id="myinfo">
                <div class="alert alert-danger" role="alert">
                    <?= $msg ?>
                </div>
            </div>
            <script>
                setTimeout(function(){
                    $('#myinfo').slideUp();
                }, 3000);
            </script>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-6">

                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">會員登入</h3></div>
<!--                    <div>--><?php //echo hash('sha256', 'dfdfgdfgdd') ?><!--</div>-->
                    <div class="panel-body">

                        <form method="post" onsubmit="return checkForm();">
                            <div class="form-group">
                                <label for="email">**電郵帳號</label> <span class="red">請填寫正確的 email</span>

                                <input type="text" class="form-control" id="email" name="email" placeholder="請輸入你的Email"
                                value="<?= empty($_POST['email']) ? '' : htmlentities($_POST['email']) ?>">
                            </div>
                            <div class="form-group">
                                <label for="password">**密碼</label> <span class="red">請填寫密碼</span>
                                <input type="password" class="form-control" id="password" name="password"
                                       value="<?= empty($_POST['password']) ? '' : htmlentities($_POST['password']) ?>">

                            </div>
                            <button type="submit" class="btn btn-primary pull-right">登入</button>
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <script>

        function checkForm(){

            var isPass = true;
            var email = $('#email');
            var password = $('#password');


            email.prev().hide();
            password.prev().hide();

            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

            if(! pattern.test(email.val())){
                email.prev().show();
                isPass = false;
            }


            if(password.val().length < 3 ){
                password.prev().show();
                isPass = false;
            }
            return isPass;
        }

    </script>
<?php include __DIR__ . '/__page_foot.php' ?>