<?php
require __DIR__ . '/__connect_db.php';
$pname = 'cart_list';

if(empty($_SESSION['cart'])){
    $has_data = false;
} else {
    $keys = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM `products` WHERE `sid` IN (%s)", implode(',', $keys));

    $rs = $mysqli->query($sql);

    while($row = $rs->fetch_assoc()){
        $row['qty'] = $_SESSION['cart'][$row['sid']]; // 取得某項商品的數量
        $c_prod[ $row['sid'] ] = $row;
    }
    $has_data = true;
//print_r($c_prod);
//exit;
}

?>
    <style>
        .del {
            color: red;
            cursor: pointer;
        }

    </style>
<?php include __DIR__. '/__page_head.php' ?>

<div class="container">
    <?php include __DIR__. '/__navbar.php' ?>


        <div class="col-md-12">
            <?php if($has_data): ?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>移除</th>
                    <th>書號</th>
                    <th>書名</th>
                    <th>封面</th>
                    <th>價格</th>
                    <th>數量</th>
                    <th>小計</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($c_prod as $item): ?>
                <tr data-sid="<?= $item['sid'] ?>">
                    <th>
                        <span class="glyphicon glyphicon-remove del"></span>
                    </th>
                    <th><?= $item['book_id'] ?></th>
                    <td><?= $item['bookname'] ?></td>
                    <td><img src="imgs/small/<?= $item['book_id'] ?>.jpg" alt="<?= $item['bookname'] ?>"></td>
                    <td class="price" data-val="<?= $item['price'] ?>"></td>
                    <td>
                        <select class="qty">
                            <?php for($i=1; $i<=20; $i++): ?>
                            <option value="<?=$i?>" <?= $i==$item['qty'] ? 'selected' : ''?>><?=$i?></option>
                            <?php endfor; ?>
                        </select>
                    </td>
                    <td class="sub-total"></td>

                </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
                <div class="alert alert-info" role="alert">總計: <strong id="amount"></strong></div>

                <?php if(isset($_SESSION['user'])): ?>
                    <a class="btn btn-primary pull-right" href="cart_confirm.php">進入結帳</a>
                <?php else: ?>
                    <a class="btn btn-danger pull-right" href="login.php">請先登入會員再結帳</a>
                <?php endif; ?>

            <?php else: ?>
                <div class="alert alert-danger" role="alert">購物車裡沒有商品</div>
            <?php endif; ?>


        </div>



</div>
    <script>
        var dallorCommas = function(n){
            return '$ ' + n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        };
        $('.del').click(function(){
            var tr = $(this).closest('tr');
            var sid = tr.attr('data-sid');

            $.get('add_to_cart.php', {sid:sid}, function(data){
                // location.href = location.href; // reload page
                calItems(data);
                tr.remove();
                calTotalAmount();
            }, 'json');
        });

        $('.qty').on('change', function(){
            var tr = $(this).closest('tr');
            var sid = tr.attr('data-sid');
            var qty = $(this).val();
            // console.log( $(this).val(), tr.attr('data-sid') );

            $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
                var sub = tr.find('.sub-total');
                var price = tr.find('.price').attr('data-val');
                sub.text( dallorCommas(qty * price) );
                calItems(data);
                calTotalAmount();
            }, 'json');
        });

        $('.sub-total').each(function(){
            var tr = $(this).closest('tr');
            var price_td = tr.find('.price');
            var price = price_td.attr('data-val');
            var qty = tr.find('.qty').val();
            price_td.text( dallorCommas( price ) );
            $(this).text( dallorCommas( price*qty ) );
        });

        // 計算總價
        var calTotalAmount = function(){
            var t = 0;
            $('.sub-total').each(function(){
                var tr = $(this).closest('tr');
                var price_td = tr.find('.price');
                var price = price_td.attr('data-val');
                var qty = tr.find('.qty').val();
                t += price*qty;
            });

            $('#amount').text( dallorCommas(t) );
        };
        calTotalAmount();
    </script>
<?php include __DIR__. '/__page_foot.php' ?>