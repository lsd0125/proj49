<?php
require __DIR__ . '/__connect_db.php';

$result = array(
        'success' => false,
        'info' => '沒有輸入資料',
);

if( isset($_POST['email']) ){

    $hash = sha1($_POST['email']. uniqid());
    $sql = "INSERT INTO `members`(
         `email`,
         `password`, 
         `mobile`, 
         `address`, 
         `birthday`, 
         `nickname`, 
         `hash`, 
         `created_at`
          ) VALUES ( ?, ?, ?, ?, ?, ?, '$hash', NOW() )";
    
    $stmt = $mysqli->prepare($sql);
    if($mysqli->error){
        echo $mysqli->error;
        exit;
    }
    $stmt->bind_param("ssssss",
        $_POST['email'] ,
        sha1($_POST['password']) ,
        $_POST['mobile'],

        $_POST['address'],
        $_POST['birthday'],
        $_POST['nickname']
    );

    $stmt->execute();

/*
    if($stmt->error){
        echo $stmt->error;
        exit;
    }
*/
    //echo $sql;
    $affected_rows = $stmt->affected_rows;
    if($affected_rows==1){
        $result['success'] = true;
        $result['info'] = '會員註冊完成';
    }else if($affected_rows==-1){
        $result['info'] = 'email 已經被使用過了';
    }


}

echo json_encode($result);
